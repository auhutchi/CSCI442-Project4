# Project 4

A simulation of a trival OS memory manager.

Name: Austin Hutchison

	List of Files:
		/src/flag_parser/flag_parser: 	A struct that parses input flags and stores them
		/src/frame/frame:		A class that represents a Frame
		/src/page/page:			A class that represents a single page of a process
		/src/page_table/page_table: 	A struct representing a page_table
		/src/physical_address/physical_address:  A class that  represents a physical address
		/src/process/process:		A class that represents a process in a similified memory simulation
		/src/simulation/simulation:	A class that represents a memory simulation. It loads in files and simulates memory access.
		/src/virtual_address/virtual_address:	A class that represent a virtual address
		
		/inputs/process_1:		Represents a process image
		/inputs/process_2:		Represents a process image
		/inputs/process_3:		Represents a process image 
		/inputs/sim_1:			A simulation file, provides memory access instructions
		/inputs/sim_2:			A simulation file, provides memory access instructions
		/inputs/sim_3:			A simulation file, specifically created to display Belady's Anomaly.
	
	Unsual Interesting Features:
		NONE


	Approx Num of hours: 20hrs


	Belady's Anomaly:
		Belady's Anomaly is when increasing the number of page frames the amount of page faults increase. This is only
		caused by some patterns	of page requests. I have tried many different amount of page frames for both sim_1 and
		sim_2 but I couldn't find an Belady Anomaly. Maybe I have wrong implemenetation but my outputs do match the few 
		example outputs that is posted. However, I created a 3rd sim file which will display Belady's Anomaly between #
		of frames 3 and 4. An example is displayed below. Belady's Anomaly occurs because FIFO only cares about how old
		the page is. So if repeat pages occur at just the right pattern, mainly more frequently, then the page may get 
		knocked out at different times than the smaller page and potentially cause more faults.

	Example:

	auhutchi@bb136-21:~/Documents/csci442/project-4$ ./mem-sim ./inputs/sim_3 -s FIFO -f 3

		Process 10:     ACCESSES: 12    FAULTS: 9       FAULT RATE: 75  RSS: 3
		
		Total memory access:            12
		Total page faults:              9
		Free frames remaining:          509
	
	auhutchi@bb136-21:~/Documents/csci442/project-4$ ./mem-sim ./inputs/sim_3 -s FIFO -f 4

		Process 10:     ACCESSES: 12    FAULTS: 10      FAULT RATE: 83.3333     RSS: 4
	
		Total memory access:            12
		Total page faults:              10
		Free frames remaining:          508

