/**
 * This file contains implementations for methods in the Page class.
 *
 * You'll need to add code here to make the corresponding tests pass.
 */

#include "page/page.h"
#include <vector>
#include <iostream>

using namespace std;


// Ensure PAGE_SIZE is initialized.
const size_t Page::PAGE_SIZE;


Page* Page::read_from_input(std::istream& in) {
	if (in.peek() == EOF) {
		return nullptr;
	}
	vector<char> inBytes;
	for (unsigned i = 0; i < PAGE_SIZE; i++) {
		if(in.peek() != EOF){
			inBytes.push_back(in.get());
	} else {
			break;
		}
	}
	Page* newPage = new Page(inBytes);
  return newPage;
}


size_t Page::size() const {
  return bytes.size();
}


bool Page::is_valid_offset(size_t offset) const {
  	
	if (offset < bytes.size()) {
		return true;
	}else {
		return false;
	}
}


char Page::get_byte_at_offset(size_t offset) {
	return bytes[offset];
}
