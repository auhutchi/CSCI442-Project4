/**
 * This file contains implementations for methods in the flag_parser.h file.
 *
 * You'll need to add code here to make the corresponding tests pass.
 */

#include "flag_parser/flag_parser.h"
#include <iostream>

#include <vector>
#include <getopt.h>
#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include <string>
#include <vector>


using namespace std;


void print_usage() {
  cout <<
      "Usage: mem-sim [options] filename\n"
      "\n"
      "Options:\n"
      "  -v, --verbose\n"
      "      Output information about every memory access.\n"
      "\n"
      "  -s, --strategy (FIFO | LRU)\n"
      "      The replacement strategy to use. One of FIFO or LRU.\n"
      "\n"
      "  -f, --max-frames <positive integer>\n"
      "      The maximum number of frames a process may be allocated.\n"
      "\n"
      "  -h --help\n"
      "      Display a help message about these flags and exit\n"
      "\n";
}


bool parse_flags(int argc, char** argv, FlagOptions& flags) {

  // Alot of copy an past

   // An array of long-form options.
  static struct option long_options[] = {
    {"strategy",required_argument, NULL, 's'},
    {"max-frames",required_argument, NULL, 'f'},
    {"verbose",	no_argument, NULL, 'v'},
    {"help", 	no_argument, NULL, 'h'},
   
    // Terminate the long_options array with an object containing all zeroes.
    {0, 0, 0, 0}
  };

  // getopt_long parses one argument at a time. Loop until it tells us that it's
  // all done (returns -1).
  while (true) {
    // getopt_long stores the latest option index here,you can get the flag's
    // long-form name by using something like long_options[option_index].name
    int option_index = 0;

    // Process the next command-line flag. the return value here is the
    // character or integer specified by the short / long options.
    int flag_char = getopt_long(
        argc,           // The total number of arguments passed to the binary
        argv,           // The arguments passed to the binary
        "s:f:vh",     // Short-form flag options
        long_options,   // Long-form flag options
        &option_index); // The index of the latest long-form flag

    // Detect the end of the options.
    if (flag_char == -1) {
      break;
    }
		string testopt;
		string fifostr = "FIFO";
		string lrustr = "LRU";
    switch (flag_char) {
    case 0:
      cout << "Saw --" << long_options[option_index].name << " flag" << endl;
      break;

    case 's':
		testopt = optarg;
		if (testopt == fifostr){
			flags.strategy = ReplacementStrategy::FIFO;
		} else if (testopt == lrustr){
			flags.strategy = ReplacementStrategy::LRU;
		} else {
			cout << "Error: Wrong type of strategy" << endl;
			return false;
		}
      break;

    case 'f':
		if (atoi(optarg) > 0) {
      		flags.max_frames = atoi(optarg);
		} else {
			return false;
		}
      break;
    case 'v':
		flags.verbose = true;	
	break;
    case 'h':
      		print_usage();
		return false;
      break;

     case '?':
      // This represents an error case, but getopt_long already printed an error
      // message for us.
	return false;
      break;

    default:
      // This would only happen if a flag hasn't been handled, or if you're not
      // detecting -1 (no more flags) correctly.
      exit(EXIT_FAILURE);
    }
  }


  // Print any other command line arguments that were not recognized as flags.
if (optind < argc) {
    	for (int i = optind; i < argc; i++) {
      			flags.filename = argv[i];
    	}
  } else {
	cout << "No file name provided" << endl;
	return false;
}
  return true;
}
