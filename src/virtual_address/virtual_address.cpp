/**
 * This file contains implementations for methods in the VirtualAddress class.
 *
 * You'll need to add code here to make the corresponding tests pass.
 */

#include "virtual_address/virtual_address.h"
#include <math.h>
#include <bitset>
using namespace std;


VirtualAddress VirtualAddress::from_string(int process_id, string address) {
	bitset<PAGE_BITS+OFFSET_BITS> addyBits(address);
	unsigned long pageNum = (addyBits>>OFFSET_BITS).to_ulong();
	unsigned long offsetAmt = ((addyBits<<PAGE_BITS)>>PAGE_BITS).to_ulong();
  return VirtualAddress(process_id, pageNum, offsetAmt);
}


string VirtualAddress::to_string() const {
	int pageAddy = page;
	bitset<PAGE_BITS> imAbitsetPage(pageAddy);
	bitset<OFFSET_BITS> imAbitsetOffset(offset);
	return imAbitsetPage.to_string() + imAbitsetOffset.to_string();
 }


ostream& operator <<(ostream& out, const VirtualAddress& address) {
	out << "PID " << address.process_id << " @ " << address.to_string() << " [page: " << address.page << "; offset: " << address.offset << "]";
	return out;
}
