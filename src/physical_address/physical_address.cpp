/**
 * This file contains implementations for methods in the PhysicalAddress class.
 *
 * You'll need to add code here to make the corresponding tests pass.
 */

#include "physical_address/physical_address.h"
//#include <sstream>
#include <bitset>
using namespace std;


string PhysicalAddress::to_string() const {
	bitset<FRAME_BITS> imAbitsetFrame(frame);
	bitset<OFFSET_BITS> imAbitsetOffset(offset);
	return imAbitsetFrame.to_string() + imAbitsetOffset.to_string();
}


ostream& operator <<(ostream& out, const PhysicalAddress& address) {
	out << address.to_string() << " [frame: " << address.frame << "; offset: " << address.offset << "]";
  	return out;
}
