/**
 * This file contains implementations for the methods defined in the Simulation
 * class.
 *
 * You'll probably spend a lot of your time here.
 */

#include "simulation/simulation.h"
#include "virtual_address/virtual_address.h"
#include "physical_address/physical_address.h"
#include "frame/frame.h"
#include <string>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <bitset>

using namespace std;


void Simulation::run(FlagOptions flagOpts) {
	
	flags = flagOpts;	

// Read sim file	
	// Open simulation file
	ifstream simFile(flags.filename.c_str());

	// Check for error
	if (!simFile) {
		cerr << "Unable to read from " << flags.filename.c_str() << endl;
		exit(EXIT_FAILURE);
	}
	
	
	// Get the simulator params
	simFile >> numProcs;

// Read in Processes
	for (unsigned i = 0; i < numProcs; i++) {
		
		// Get proc parms
		unsigned procId;
		string procFileName;
		simFile	>> procId
			>> procFileName;

		// Open simulation file
		ifstream procFile(procFileName.c_str());
	
		// Check for error
		if (!procFile) {
			cerr << "Unable to read from " << procFileName.c_str() << endl;
			exit(EXIT_FAILURE);
		}
		
		// Create a process
		Process* newProc = Process::read_from_input(procFile);
		newProc->pid = procId;
		
		// Map the proc
		procs[procId] = newProc;		
	}


// Read in the rest of the sim file and do it.

	unsigned procId;
	string addy;
	while(simFile >> procId >> addy){
		theClock++;
	VirtualAddress vAddy = VirtualAddress::from_string(procId, addy);	
	perform_memory_access(vAddy);		// Do the memory access
	}

	cout << endl << "Done !" << endl << endl;

// Print the overall stats	
	unsigned totalMemAcc = 0;
	unsigned totalPageFlt = 0;
	for (map<unsigned, Process*>::iterator itr=procs.begin(); itr!=procs.end(); ++itr) {
		cout 	<< "Process " << itr->first << ":\t"
			<< "ACCESSES: " << itr->second->memory_accesses << "\t"
			<< "FAULTS: " << itr->second->page_faults << "\t"
			<< "FAULT RATE: " << (itr->second->page_faults * 100.00) / itr->second->memory_accesses << "\t"
			<< "RSS: " << itr->second->get_rss() 
			<< endl;
		totalMemAcc = totalMemAcc + itr->second->memory_accesses;
		totalPageFlt = totalPageFlt + itr->second->page_faults;
	}
	cout 	<< endl
		<< "Total memory access:  \t\t" << totalMemAcc << endl
		<< "Total page faults:    \t\t" << totalPageFlt << endl
		<< "Free frames remaining:\t\t" << NUM_FRAMES - frames.size() << endl;
}
char Simulation::perform_memory_access(const VirtualAddress& address) {
	bool inMem = procs[address.process_id]->page_table.rows[address.page].present;
	bool full = !((procs[address.process_id]->get_rss() < flags.max_frames)&&(frames.size() < NUM_FRAMES));
	if (flags.verbose) cout << address << endl;
	if (inMem) {
		if (flags.verbose) cout << " -> IN MEMORY" << endl;
	} else if(!inMem && !full) {
		// Add frame
		if (flags.verbose) cout << " -> PAGE FAULT" << endl;
		Frame newFrame;
		newFrame.set_page(procs[address.process_id], address.page);
		frames.push_back(newFrame);
		nextFrame++;
		procs[address.process_id]->page_table.rows[address.page].present = true;
		procs[address.process_id]->page_table.rows[address.page].loaded_at = theClock;
		procs[address.process_id]->page_table.rows[address.page].frame = frames.size() - 1;
		procs[address.process_id]->page_faults++;
		
	} else {
		//page fault
		if (flags.verbose)cout << " -> PAGE FAULT" << endl;
		Simulation::handle_page_fault(procs[address.process_id],address.page);
		procs[address.process_id]->page_faults++;
	}
	PhysicalAddress pAddy(procs[address.process_id]->page_table.rows[address.page].frame, address.offset);
	if (flags.verbose) cout << " -> physical address " << pAddy << endl;
	if (flags.verbose) cout << " -> RSS: " << procs[address.process_id]->get_rss() << endl;

		return Simulation::inMemAccess(address);
  return 0;
}

// Accesses the value in memory after its been loaded in
char Simulation::inMemAccess(const VirtualAddress& address) {
	procs[address.process_id]->page_table.rows[address.page].last_accessed_at = theClock;
	bool validOffset = procs[address.process_id]->pages[address.page]->is_valid_offset(address.offset);
	procs[address.process_id]->memory_accesses++;
	if (validOffset) {
		return procs[address.process_id]->pages[address.page]->get_byte_at_offset(address.offset);
	} else {
		return 0;
	}		
}

// Replace a page if neccassary
void Simulation::handle_page_fault(Process* process, size_t page) {
  // TODO: impleVment me
	if (frames.size() < NUM_FRAMES){
		size_t oldPage;
		size_t oldFrame;
		if (flags.strategy == ReplacementStrategy::FIFO) {
			oldPage = process->page_table.get_oldest_page();
		} else if (flags.strategy == ReplacementStrategy::LRU) {

			oldPage = process->page_table.get_least_recently_used_page();
		}
		process->page_table.rows[oldPage].present = false;
		process->page_table.rows[page].present = true;
		process->page_table.rows[page].loaded_at = theClock;
		oldFrame = process->page_table.rows[oldPage].frame;
		process->page_table.rows[page].frame = oldFrame;
		frames[oldFrame].set_page(process, page);
	} else {
		cout << "ERROR: MEM FULL" << endl;	// todo
		//for (map<unsigned, Process*>::iterator itr=procs.begin(); itr!=procs.end(); ++itr) {
		//	
		//}
	}
	
}






