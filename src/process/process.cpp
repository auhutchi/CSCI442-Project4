/**
 * This file contains implementations for methods in the Process class.
 *
 * You'll need to add code here to make the corresponding tests pass.
 */

#include "process/process.h"
using namespace std;


Process* Process::read_from_input(std::istream& in) {
	vector<Page*> Pages;
	unsigned byteSize = 0;
	while (in.peek() != EOF) {
		Page* page = Page::read_from_input(in);
		Pages.push_back(page);
		byteSize = byteSize + page->size();
  	}
	Process* newProc = new Process(byteSize, Pages);
	return newProc;
}


size_t Process::size() const {
  return num_bytes;
}


bool Process::is_valid_page(size_t index) const {
  	if (pages.size() > index) {
		return true;
	} else {
		return false;
	}
}


size_t Process::get_rss() const {
  	
	return page_table.get_present_page_count();
}


double Process::get_fault_percent() const {
	if (memory_accesses != 0){
  		double cent = (page_faults * 100.00) / memory_accesses;
		return cent;
	} else {
		return 0;
	}
}
