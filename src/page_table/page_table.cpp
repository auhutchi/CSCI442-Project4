/**
 * This file contains implementations for methods in the PageTable class.
 *
 * You'll need to add code here to make the corresponding tests pass.
 */

#include "page_table/page_table.h"

using namespace std;


size_t PageTable::get_present_page_count() const {
	unsigned count = 0;
	for (unsigned i = 0; i < rows.size(); i++){
		if (rows[i].present == true){
			count++;
		}
	}
  	return count;
}


size_t PageTable::get_oldest_page() const {
	unsigned oldest = 0;
	while(oldest < rows.size()) {
		if (rows[oldest].present == true) {
			break;
		} else {
			oldest++;
		}
	}
	for (unsigned i = 0; i < rows.size(); i++){
		if (rows[i].present == true){
			if (rows[oldest].loaded_at > rows[i].loaded_at){
				oldest = i;
			}
		}
	}
  	return oldest;
}


size_t PageTable::get_least_recently_used_page() const {
	unsigned  lru =  0;
	while(lru < rows.size()) {
		if (rows[lru].present == true) {
			break;
		} else {
			lru++;
		}
	}
	for (unsigned i = 0; i < rows.size(); i++){
		if (rows[i].present == true){
			if (rows[lru].last_accessed_at >  rows[i].last_accessed_at){
				lru = i;
			}
		}
	}
  	return lru;
}
